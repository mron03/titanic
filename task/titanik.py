import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def find_title_within_name(full_name: str) -> str:
    titles = ['Mr.', 'Mrs.', 'Miss.']
    found_title = next((title for title in titles if title in full_name), None)
    return found_title

def get_filled():
    df = get_titatic_dataframe()
    df['Title'] = df['Name'].map(find_title_within_name)
    filtered_data = df[df['Title'].notnull()]

    age_medians = filtered_data.groupby('Title')['Age'].median()
    missing_age_counts = filtered_data['Age'].isna().groupby(filtered_data['Title']).sum()
    
    res = [(title, missing_age_counts[title], age_medians[title]) for title in ['Mr.', 'Mrs.', 'Miss.']]

    return res

